from datetime import datetime
import time
from neopixel import *
import sys
import urllib2
import re

#LED strip configuration:
LED_COUNT      = 80      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 125     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_STRIP      = ws.WS2811_STRIP_GRB   # Strip type and colour ordering



# Define functions which animate LEDs in various ways.
def set_led(position,strip, color, wait_ms=10):
        """light up one pixel."""
        strip.setPixelColor(position, color)
        strip.show()
        time.sleep(wait_ms/1000.0)





#compile the URL to get weather
airport_dict = {"KCAV": {"LED":17},"KFXY": {"LED":19},"KMCW": {"LED":20},"KHPT": {"LED": 16},"KIFA": {"LED": 15},"KAMW": {"LED": 13},"KIKV": {"LED": 12},"KDSM": {"LED": 11},"KOXV": {"LED": 10},"KPEA": {"LED": 6},"KTNU": {"LED": 5},"KGGI": {"LED": 4},"KMIW" : {"LED": 2},"KALO": {"LED": 0},"KCCY": {"LED":22},"KDEH": {"LED":24},"KOLZ": {"LED":25},"KIIB": {"LED":26},"KVTI": {"LED":27},"KCID": {"LED":28},"KIOW": {"LED":29},"KOOA": {"LED": 7},"KOTM": {"LED": 8},"KAWG": {"LED":30},"KMUT": {"LED":31},"KMLI": {"LED":43},"KDVN": {"LED":42},"KCWI": {"LED":44},"KMXO": {"LED":33},"KDBQ": {"LED":40},"KPVB": {"LED":39},"KMRJ": {"LED":38},"KPDC": {"LED":35},"KOVS": {"LED":36},"KLNR": {"LED":37},"KC29": {"LED":50},"KMSN": {"LED":51},"KRYV": {"LED":52},"KUES": {"LED":72},"KMWC": {"LED":73},"KMKE": {"LED":70},"K57C": {"LED":71},"KBUU": {"LED":69},"KENW": {"LED":68},"KUGN": {"LED":67},"KPWK": {"LED":66},"KORD": {"LED":65},"K06C": {"LED":64},"KDPA": {"LED":63},"KLOT": {"LED":62},"KJOT": {"LED":61},"KC09": {"LED":60},"KARR": {"LED":59},"KDKB": {"LED":58},"KRPJ": {"LED":56},"KRFD": {"LED":55},"KJVL": {"LED":54},"KEFT": {"LED":48},"KFEP": {"LED":47},"KSQI": {"LED":45}}
url = 'http://www.aviationweather.gov/metar/data?ids='
for airport in airport_dict:
        url = url + airport + ","
        airport_dict[airport]["condition"] = ""
        airport_dict[airport]["update"] = 0
url = url[:-1]
url = url + "&format=raw&hours=0&taf=off&layout=off"

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, LED_STRIP)
# Intialize the library (must be called once before other functions).
strip.begin()

while True:
        log_file = open("metar_log_py.log","a")
        logdata = ""

        #HTTP GET the data from URL compiled earlier, filter for METAR DATA
        try: 
                contents = urllib2.urlopen(url).read()
        except urllib2.HTTPError, e:
                print('HTTPError = ' + str(e.code))
                time.sleep(60)
                continue
        except urllib2.URLError, e:
                print('URLError = ' + str(e.reason)) 
                time.sleep(60)
                continue
        except:
                print("Unknown error during http fetch.")
                continue
        
        try:
                contents = contents.split("<!-- Data starts here -->")[1]
        except IndexError:
                print("Content split failed due to index error")
        except:
                print("Contents split failed for unknown reason")
        try: contents = contents.split("<!-- Data ends here -->")[0]
        except IndexError:
                print("Content split failed due to index error")
        except:
                print("Contents split failed for unknown reason")
        HTML_list = contents.splitlines()

        #strip the html from the METAR data
        METAR_list = []
        for line in HTML_list:
                if line.count("<code>"): 
                        line = line.split("<code>")[1]
                else: 
                        continue
                line = line.split("</code>")[0]
                METAR_list.append(line)
                
        #splits each METAR into it's elements, searches for visibility and ceiling, then computes flight condition and stores in airport_dict
        condition_dict = {}
        for METAR in METAR_list:
        #        logdata = logdata + METAR
                condition = ""
                vis = -1
                ceiling = 999
                METAR_elements = METAR.split()
                for element in METAR_elements:
                        #find visibility, if less than 1 set to 0
                        if re.search("\dSM",element): 
                                if re.search("/", element[:-2]):
                                        vis = 0 
                                else:
                                        vis = int(element[:-2])
                        #find cloud ceiling
                        elif re.search("((BKN|OVC)(\d{3}))",element):
                                ceiling = int(re.findall(r'\d+',element[-3:])[0])
                if   vis > 5 and ceiling > 30:
                        condition = "VFR"
                elif vis > 3 and ceiling > 10:
                        condition = "MVFR"
                elif vis > 1 and ceiling > 5:
                        condition = "IFR"
                elif vis > -1:
                        condition = "LIFR"
                else:
                        condition = "UNKNOWN"
                
                condition_dict[METAR_elements[0]]=condition

        
        for airport in airport_dict:
                #check if airport already has a conditon, set condition if it did not or if it changed
                if airport in condition_dict.keys():
                        if airport_dict[airport]["condition"] != condition_dict[airport]:
                                airport_dict[airport]["condition"] = condition_dict[airport]
                                airport_dict[airport]["update"] = 1
                        else:
                                airport_dict[airport]["update"] = 0
                else:
                        if airport_dict[airport]["condition"] != "UNKNOWN":
                                airport_dict[airport]["condition"] = "UNKNOWN"
                                airport_dict[airport]["update"] = 1
                        else:
                                airport_dict[airport]["update"] = 0
                #set airport color on map, make change only if condition updated, log actions
                position = airport_dict[airport]["LED"]
                condition = airport_dict[airport]["condition"]
                #print("%s %s\n", (position,airport_dict[airport]["condition"] ))
                mycolor = [0,0,0]
                if airport_dict[airport]["update"] == 1:
                        if condition == 'VFR':
                                set_led(position,strip,Color(20,0,0))
                                mycolor = [20,0,0]
                        elif condition == 'MVFR':
                                set_led(position,strip,Color(0,0,30))
                                mycolor = [0,0,30]
                        elif condition == 'IFR':
                                set_led(position,strip,Color(0,30,0))
                                mycolor = [0,30,0]
                        elif condition == 'LIFR':
                                set_led(position,strip,Color(0,30,20))
                                mycolor = [0,30,20]
                        else:
                                set_led(position,strip,Color(0,0,0))
                        logdata = logdata + str(airport) + " " + str(position) + " " +  condition + " " + str(mycolor) + "\n"


        now = datetime.now()
        log_file.write("\n%s\n%s" %(now, logdata))
        log_file.close()

        time.sleep(180)


